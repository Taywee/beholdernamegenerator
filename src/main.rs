use rand::distributions::Bernoulli;
use rand::prelude::*;
use serde::Deserialize;
use serde_json::Result;

const JSON: &str = include_str!("sounds.json");

#[derive(Deserialize, Debug)]
struct Sounds {
    vowels: Vec<String>,
    consonants: Vec<String>,
}

enum Phoneme {
    Vowel,
    Consonant,
}

fn main() -> Result<()> {
    let sounds: Sounds = serde_json::from_str(JSON)?;
    let mut rng = rand::thread_rng();

    // Boolean 50% distribution
    let coin = Bernoulli::new(0.5).unwrap();

    let name: Vec<String> = (0..2)
        .map(|_| {
            let mut structure = vec![Phoneme::Vowel, Phoneme::Consonant];
            // Make a 3-element structure
            if coin.sample(&mut rng) {
                structure.push(if coin.sample(&mut rng) {
                    Phoneme::Consonant
                } else {
                    Phoneme::Vowel
                })
            }
            structure.shuffle(&mut rng);

            structure
                .into_iter()
                .fold(String::new(), |mut acc, phoneme| {
                    acc.push_str(match phoneme {
                        Phoneme::Vowel => sounds.vowels.choose(&mut rng).unwrap(),
                        Phoneme::Consonant => sounds.consonants.choose(&mut rng).unwrap(),
                    });
                    acc
                })
        })
        .collect();

    println!("{}", name.join(" "));

    Ok(())
}
